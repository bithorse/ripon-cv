<script>
$(function(){
	$("li.parent_li:first-child").addClass("active");
	$('.parent_menu_link').on("click", function(){
		// Store the values
		var data_id = $(this).attr("menu-id");
		var parent_no = $(this).closest('li.parent_li').index() + 1;

		// Remove the selected element
		$("li.parent_li").removeClass("active");
		

		$(this).closest('li.parent_li').addClass("active");
		
		return false;
	});
	
	$('.child_menu_link').on("click", function(){
		// Store the values
		var data_id = $(this).attr("menu-id");
		var child_no = $(this).closest('li.child_li').index() + 1;

		// Remove the selected element
		$("li.parent_li").removeClass("active");
		$('li.child_li').removeClass("active");

		$(this).closest('li.parent_li').addClass("active");
		$(this).closest('li.child_li:nth-child('+child_no+')').addClass("active");
		return false;
	});
});
</script>