<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>This is title</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/css/font-awesome.min.css">
	
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />   
     	<!---Date picker --->
	  <link rel="stylesheet" href="date_picker/css/jquery-ui.css">
	  <script src="js/jQuery-2.1.4.min.js"></script>  
	  <script src="date_picker/js/jquery-ui.js"></script>
	  <script>
		  $( function(){
			
			$( "#datepicker" ).datepicker({
			  dateFormat: "dd-mm-yy"
			});
			
			$( "#datepicker1" ).datepicker({
			  dateFormat: "dd-mm-yy"
			});
			
		  });
	  </script>	  
	  
	<!---End Date picker --->
   	<script src="js/tinymce/tinymce.min.js"></script>
	<script>tinymce.init({ selector:'textarea' });</script>
	
	

		<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="column">
		    <div class="fix header_area">
				<div class="header">
					<h2>CV input Form</h2>
				</div>
		    </div>

			<div class="fix maincontent_area">
				<div class="fix structure maincontent">
				  <div class="section_area">	
								
							 <div class="box box-info" style="width:900px;padding:5px;background:#E8E8FF;">
								<div class="box-header">
								  <i class=""></i>
								  <h3 style="text-align:center;margin-bottom:60px;border-bottom:2px solid #999;padding:5px">Table For Adding Personal Information</h3>
								 
								</div>	
							  <div class="box-body">				
								  <form action="" method="POST" name="form" enctype="multipart/form-data">
								  
									<div class="form-group"> 
									   <p style="color:red;">Full Name:</p>
									   <input type="text" class="form-control"  name="full_name" placeholder="Full Name:" required="required" />
									</div> 
									<div class="form-group"> 
									   <p style="color:red;">Father's Name :</p>
									   <input type="text" class="form-control"  name="fathers_name" placeholder="Father's Name :" required="required" />
									</div> 
									<div class="form-group"> 
									   <p style="color:red;">Mother's Name :</p>
									   <input type="text" class="form-control"  name="mothers_name" placeholder="Mother's Name :" required="required" />
									</div> 
									<div class="form-group"> 
									   <p style="color:red;">Permanent Address :</p>
									   <input type="text" class="form-control"  name="permanent_address" placeholder="Permanent Address :" required="required" />
									</div> 
									<div class="form-group"> 
									   <p style="color:red;">Present Address :</p>
									   <input type="text" class="form-control"  name="present_address" placeholder="Present Address :" required="required" />
									</div>
									
									<div class="form-group"> 
									   <p style="color:red;">Date of Birth :</p>
									    <input type="text" class="form-control" id="datepicker"  name="date_birth" required="required" />
									</div> 
									<div class="form-group"> 
									   <p style="color:red;">Gender :</p>
									   <select name="gender" class="selectpicker">
											<option>male</option>
											<option>female</option>
									   </select>
									</div> 
									<div class="form-group"> 
									   <p style="color:red;">Blood Group :</p>
									   <input type="text" class="form-control"  name="blood_group" placeholder="Blood Group :" required="required" />
									</div> 
									<div class="form-group"> 
									   <p style="color:red;">Marital Status :</p>
									   <select name="gender" class="selectpicker">
											<option>married</option>
											<option>unmarried</option>
									   </select>
									</div> 
									<div class="form-group"> 
									   <p style="color:red;">Nationality :</p>
									   <input type="text" class="form-control"  name="nationality" placeholder="Nationality :" required="required" />
									</div> 
									
									<div class="form-group"> 
									   <p style="color:red;">Religion :</p>
									   <input type="text" class="form-control"  name="religion" placeholder="Religion :" required="required" />
									</div>
									
									<div class="form-group"> 
									   <p style="color:red;">National Id No :</p>
									   <input type="text" class="form-control"  name="national_id" placeholder="National Id No:" required="required" />
									</div> 
									
									<div class="form-group"> 
									   <p style="color:red;">My Traits:</p>
									   <input type="text" class="form-control"  name="my_traits" placeholder="My Traits:" required="required" />
									</div> 
									
									<div class="form-group"> 
									   <p style="color:red;">Hobbies :</p>
									   <input type="text" class="form-control"  name="hobbies" placeholder="Hobbies :" required="required" />
									</div> 
									<div class="form-group">
									   <p style="color:red;">Experience:</p>
									   <textarea  name="experience"></textarea>
									</div> 
									 
									
									
									 <div class="form-group">
									    <p style="color:red;">Select an image:</p>
										<input type="file" required="required" class="form-control" placeholder="select an image"  name="fileToUpload" />
									</div>   
									<div class="form-group"> 
										<p style="color:red;">Career Objectives:</p>
									   <textarea  name="carrer_obj"></textarea>
									</div> 
									 <div class="form-group">
										 <p style="color:red;">Date:</p>
										 <input type="text" class="form-control" id="datepicker1"  name="date" required="required" />
									</div>  
									 <p style="color:red;">Academic Qualification:</p>
									 <div class="form-group">
										  <table class="table" style="border:1px solid #999">
											<thead>
											  <tr>
												<th>Exam Tile</th>
												<th>Name of examination</th>
												<th>Board/University</th>
												<th>Result</th>
												<th>Passing Year</th>
											  </tr>
											</thead>
											<tbody>
											  <tr>
												<td>
												  <h4>SSC/Dakhil</h2>
												</td>
												<td>
												  <select name="course_name">
													 <option value="exam_name" >SSC</option>
													 <option value="exam_name">Dakhil</option>	
													 <option value="exam_name">O Level</option>	
												  </select>
												
												
												
												</td>
												<td>
												
												<select name="course_name">
													 <option value="exam_name" >Dhaka</option>
													 <option value="exam_name">Mymensingh</option>	
													 <option value="exam_name">Syllet</option>	
													 <option value="exam_name">Rajshahi</option>	
												  </select>
												
												</td>
												<td>
													
													<input class="form-control" type="number" value="4" id="example-number-input">
												
												</td>
												<td><input class="form-control" type="number" value="1980" id="example-number-input"></td>
											  </tr>
											  <tr>
												<td><h4>HSC/Alim</h2></td>
												<td>
												   <select name="course_name">
													 <option value="exam_name" >HSC</option>
													 <option value="exam_name">Alim</option>	
													 <option value="exam_name">A Level</option>	
												  </select>
												</td>
												<td>
												
												<select name="course_name">
													 <option value="exam_name" >Dhaka</option>
													 <option value="exam_name">Mymensingh</option>	
													 <option value="exam_name">Syllet</option>	
													 <option value="exam_name">Rajshahi</option>	
												  </select>
												
												</td>
												
												<td><input class="form-control" type="number" value="4" id="example-number-input"></td>
												<td><input class="form-control" type="number" value="1980" id="example-number-input"></td>
											  </tr>
											  <tr>
												<td><h4>B.Sc</h2></td>
												<td>
												 <select name="course_name">
													 <option value="exam_name" >Bangla</option>
													 <option value="exam_name">English</option>	
													 <option value="exam_name">Science</option>	
													 <option value="exam_name">Computer Science & Engineering</option>	
													 <option value="exam_name">EEE</option>	
													 <option value="exam_name">Civil</option>	
												  </select>
												</td>
												<td>
													<input type="text" class="form-control"  name="title" placeholder="Public/national university" required="required" />
												</td>
												<td><input class="form-control" type="number" value="5" id="example-number-input"></td>
												<td><input class="form-control" type="number" value="1980" id="example-number-input"></td>
											  </tr>
											</tbody>
										  </table>
									 </div>
									 
									
									<div class="box-footer clearfix">
										  <a href="index.php" class="pull-left btn btn-primary">Back</a>
										 <input type="submit" name="submit" value="Submit" class="pull-right btn btn-primary">
									</div>
								  </form>
							  </div>
						
					   </div>
					

					</div>
				</div>
			</div>
			<div class="fix footer_area">
				 <div class="fix structure footer">
					<p>Copyright &copy; All Rights Reserved By RIPON.</p>
				 </div>
			</div>
	    </div>				
			
    </body>
</html>
