	
	
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />   
   	<!---Date picker --->
	  <link rel="stylesheet" href="date_picker/css/jquery-ui.css">
	  <script src="js/jQuery-2.1.4.min.js"></script>  
	  <script src="date_picker/js/jquery-ui.js"></script>
	  <script>
		  $( function(){
			
			$( "#datepicker" ).datepicker({
			  dateFormat: "dd-mm-yy"
			});
			
		  });
	  </script>	  
	  
	<!---End Date picker --->
	<script src="js/tinymce/tinymce.min.js"></script>
	<script>tinymce.init({ selector:'textarea' });</script>
       
        <!-- Main content -->
        <section class="content">		
          <!-- Main row -->
          <div class="row">		  
            <!-- Left col -->
            <section class="col-lg-7 connectedSortable">			
			<!-- quick email widget -->
             <div class="box box-info">
                <div class="box-header">
                  <i class=""></i>
                  <h3 class="box-title">Table For Add Course Lecture Doc!</h3>
                  <!-- tools box -->
                <div class="pull-right box-tools">
                   <button class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                </div><!-- /. tools -->
                </div>
                <?php
                  if(isset($_POST['submit']))
                  {
                  	include("includes/database.php");
					
					$course_name=$_POST['course_name'];  
					$title=$_POST['title'];  
					$date=$_POST['date'];  
					$ppt_lecture_num=$_POST['ppt_lecture_num'];  
					$pdf_lecture_num=$_POST['pdf_lecture_num']; 
					

					
					
					
					
					//$id_ppt=rand(1000,999999);
					//$id_pdf=rand(1000,999999);
					
					$lecture = "Lecture";
					$success_pdf = 0;
					$success_ppt = 0;
                           
					$des_ppt="Doc_files/".$lecture.'_'.$ppt_lecture_num.'_'.$course_name.".ppt";
					$filename_ppt=$_FILES['ppt']['name'];  //you can change this with your filename
					$src_ppt=$_FILES['ppt']['tmp_name'];  //you can change this with your filename
					$allowed='pdf,ppt';  //which file types are allowed seperated by comma

					$extension_allowed= explode(',', $allowed);
					$file_extension=  pathinfo($filename_ppt, PATHINFO_EXTENSION);
										
					//echo $filename_ppt;
					//echo $file_extension;
					
					//echo "<pre>";
					
					//print_r($extension_allowed);
					
					//echo "</pre>";
					//die();
					
					if(array_search($file_extension, $extension_allowed))
					{
						//echo "$extension_allowed allowed for uploading file";
						copy($src_ppt,$des_ppt);
						$success_ppt = 1;
					}
					
					//echo $success_ppt;
					//die();

					$des_pdf="Doc_files/".$lecture.'_'.$pdf_lecture_num.'_'.$course_name.".pdf";
					$filename_pdf=$_FILES['pdf']['name'];  //you can change this with your filename
					$src_pdf=$_FILES['pdf']['tmp_name'];  //you can change this with your filename
					$allowed='ppt,pdf';  //which file types are allowed seperated by comma

					$extension_allowed=  explode(',', $allowed);
					$file_extension=  pathinfo($filename_pdf, PATHINFO_EXTENSION);
					if(array_search($file_extension, $extension_allowed))
					{
						//echo "$extension_allowed allowed for uploading file";
						copy($src_pdf,$des_pdf);
						$success_pdf = 1;
					}
					

					$ppt = $lecture.'_'.$ppt_lecture_num.'_'.$course_name.".ppt";
					$pdf = $lecture.'_'.$pdf_lecture_num.'_'.$course_name.".pdf";
					
					
	
					if( $success_ppt !=1 && $success_pdf !=1)
					{
						echo"<h3 style='color:red;'>Woops, Something went Wrong!(Please upload valid file)</h3>";
					}
					else
					{
						$sql="INSERT into course_outline set page='$course_name',title='$title',ppt='$ppt',pdf='$pdf',date='$date'";
						$result=mysqli_query($con,$sql);
						if(!$result)
						{
							echo"<h3 style='color:red;'>Woops, Something went Wrong!!!</h3>".mysqli_connect_error();
						}
						else
						{
							echo"<h3 style='color:green;'>Course Lecture Doc Successfully Added!</h3>";
						}
						
					}                  
                  }                 

                ?>
				
				
				
              <div class="box-body">				
                  <form action="" method="POST" name="form" enctype="multipart/form-data">
                     <div class="form-group">
                       <span>Power Point File(ppt): <p style="color:red;"></p></span>
						<input type="file" required="required" class="form-control" placeholder="power point file"  name="ppt" />
						<input type="text" class="form-control"  name="ppt_lecture_num" placeholder="Lecture Number:1,2,3...." required="required" />
                    </div>   
					<div class="form-group">
                       <span>PDF File(pdf): <p style="color:red;"></p></span>
						<input type="file" required="required" class="form-control" placeholder="pdf file"  name="pdf" />
						<input type="text" class="form-control"  name="pdf_lecture_num" placeholder="Lecture Number:1,2,3...." required="required" />
                    </div> 
                     <div class="form-group">
                        <p>Date: <input type="text" class="form-control" id="datepicker"  name="date" required="required" /></p>

                    </div>  
					 
					<div class="form-group">
						<h5 style="color:red;">Select Course </h3><select name="course_name">
									<option value="" >--Select Course name--</option>
									<?php 
									while($row=mysqli_fetch_assoc($sub_menu_result))
									{
										?>
									<option value="<?php echo $row['page'] ; ?>"><?php echo $row['page'] ; ?></option>	
									<?php }?>
									
								</select>
					</div> 
					
					 <div class="form-group">
						<span>Description: <p style="color:red;"></p></span>
                       <textarea  name="description"></textarea>
                    </div> 
					
					
					 <div class="form-group">
						<span>Course Title(with course Code): <p style="color:red;"></p></span>
                       <input type="text" class="form-control"  name="title" placeholder="example:Computer Fundamental(course code:cse-101)" required="required" />
                    </div> 
                     
				    <div class="box-footer clearfix">
						  <a href="index.php" class="pull-left btn btn-primary">Back</a>
                         <input type="submit" name="submit" value="Submit" class="pull-right btn btn-primary">
                     </div>
                  </form>
			  </div>
		
       </div>
	  </section><!-- /.Left col -->			
    </div> <!-- /.row (main row) -->
  </section><!-- /.content -->
</div>   



    
