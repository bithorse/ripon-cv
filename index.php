<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>This is title</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="css/style.css">
        <link rel="stylesheet" href="css/css/font-awesome.min.css">
	

		<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="column">
		    <div class="fix header_area">
				<div class="fix structure header_left">
					
					<h2>Curriculum Vitae <br>of <br>Md.Abdullah Al Mamun</h2>
					
				</div>
				<div class="fix structure header_right">
					
					<img src="images/ripon.jpg" alt="" />
					
				</div>
		    </div>
		
        
        
        
			<div class="fix maincontent_area">
				<div class="fix structure maincontent">
				  <div class="section_area">	
						<div class="section_area_1">
							<h2>Career Objectives</h2>
							<p>Achieving a dynamic & challenging job to utilize my skill & interpersonal efficiencies to serve the humanity in the best possible manner.   </p>
						</div>
						<!-- end section_area_1 --->
						
						<div class="section_area_2">
							<h2>Academic Qualification</h2>
							<table border="1">
								<tr>
									<th>Exam Title</th>
									<th>Concentration/Major</th>
									<th>Institute and Board</th>
									<th>Result</th>        
									<th>Passing Year</th>
								</tr>
								<tr>
									<td>Computer Science and Engineering (CSE)   </td>
									<td>Computer Science & Engineering  </td>
									<td>Jatiya Kabi Kazi Nazrul Islam University, Trishal, Mymensingh      </td>
									<td>CGPA: 2.85 out of 4   </td>
									<td>2012</td>
								</tr>
								<tr>
									<td>HSC   </td>
									<td>Science     </td>
									<td>Hazi Kashem Ali College, Mymensingh,Dhaka Board </td>
									<td>CGPA:4out of 5   </td>
									<td>2008  </td>
								</tr>
								<tr>
									<td>SSC </td>
									<td>Science </td>
									<td>Mukul Niketon High School, Mymensingh, Dhaka Board   </td>
									<td>CGPA:4 out of 5 </td>
									<td>2005</td>
								</tr>
						   </table>
						</div>	
						<!-- end section_area_2 --->
						<div class="section_area_3">
							<h2>Personal Information</h2>
							<table>
								<tr>
									<td>Name :</td>
									<td>Md. Abdullah Al Mamun</td>
								</tr>
								<tr>
									<td>Father's Name :</td>
									<td>Late Abdul Gani Mia</td>
								</tr>
								<tr>
									<td>Mother's Name :</td>
									<td>Mst. Momena Begum</td>
								</tr>
								<tr>
									<td>Permanent Address :</td>
									<td>
										15 Balashpur (Palpara), Mymensingh-2200,      
										Mymensingh Sadar. Mobile: 0088- 1683353016, 0088-1776 020491,
										e-mail:mamun1eng@yahoo.com, mamuneng89@gmail.com.
									</td>
								</tr>
								<tr>
									<td>Date of Birth :</td>
									<td>November 24, 1989 </td>
								</tr>
								<tr>
									<td>Gender :</td>
									<td>Male</td>
								</tr>
								<tr>
									<td>Blood Group :</td>
									<td>A+</td>
								</tr>
								<tr>
									<td>Marital Status  :</td>
									<td>Unmarried</td>
								</tr>
								<tr>
									<td>Nationality :</td>
									<td>Bangladeshi </td>
								</tr>
								<tr>
									<td>Religion :</td>
									<td>Islam</td>
								</tr>
								<tr>
									<td>National Id No :</td>
									<td>6125219257128</td>
								</tr>
							</table>
						</div>
						<!-- end section_area_3 --->
						<div class="section_area_4">
							<h2>Present Address</h2>
							<p>15 Balashpur (Palpara), Mymensingh Sadar, Mymensingh.</p>
						</div>
						<!-- end section_area_4 --->
						<div class="section_area_5">
							<h2>Permanent Address</h2>
							<p>15 Balashpur (Palpara), Mymensingh Sadar, Mymensingh.</p>
						</div>
						<!-- end section_area_5 --->
						<div class="section_area_6">
							<h2>Computer Literacy</h2>
							<p>MS Word, Excel, Access, PowerPoint,SpreadSheet,
							Networking,Graphics Design, Web Programming: HTML-5, 
							CSS , PHP(raw), Database: SQL, MySQL</p>
						</div>
						<!-- end section_area_6 --->
						<div class="section_area_7">
							<h2>Hobbies</h2>
							<ul>
								<li><i style="padding:5px;" class="fa fa-check-square-o fa-lg"></i>Reading</li>
								<li><i style="padding:5px;" class="fa fa-check-square-o fa-lg"></i>Traveling</li>
								<li><i style="padding:5px;" class="fa fa-check-square-o fa-lg"></i>Programming</li>
								<li><i style="padding:5px;" class="fa fa-check-square-o fa-lg"></i>Watching Live Cricket Match</li>
							
							</ul>
						</div>
						<!-- end section_area_7 --->
						<div class="section_area_8">
							<h2>Experiences</h2>
							<ul>
								<li><i style="padding:5px;" class="fa fa-check fa-lg"></i>Worked as web developer for 1 year in IT Bangla Firm.</li>
								<li><i style="padding:5px;" class="fa fa-check fa-lg"></i>Worked as Api developer for 2 year in IT Firm.</li>
								<li><i style="padding:5px;" class="fa fa-check fa-lg"></i>Worked as apps developer for 4 year in IT Frelance Club. </li>
								<li><i style="padding:5px;" class="fa fa-check fa-lg"></i>Worked as Software developer for 6 month in IT Bangla Firm.</li>	
							</ul>
						</div>
						<!-- end section_area_8 --->
						<div class="section_area_9">
							<h2>My Traits</h2>
							<ul>
								<li><i style="padding:5px;" class="fa fa-arrow-right fa-lg"></i>Ability to work under pressure in busy environment</li>
								<li><i style="padding:5px;" class="fa fa-arrow-right fa-lg"></i>Hard working and capable of adopting in any type of environment</li>
								<li><i style="padding:5px;" class="fa fa-arrow-right fa-lg"></i>Confident, bright, and enthusiastic</li>
								<li><i style="padding:5px;" class="fa fa-arrow-right fa-lg"></i>Strong Interpersonal & communicational skill</li>
								<li><i style="padding:5px;" class="fa fa-arrow-right fa-lg"></i>Self-motivated good social worker</li>
							</ul>
							<p>The information given above is true and accurate.</p>
							<p style="margin-top:40px;">.........................................</p>
							<p>Md. Abdullah Al Mamun </p>
						</div>
						<!-- end section_area_9 --->
						
					</div>
				</div>
			</div>
			<div class="fix footer_area">
				 <div class="fix structure footer">
					<p>Copyright &copy; All Rights Reserved By RIPON.</p>
				 </div>
			</div>
	    </div>				
			
    </body>
</html>
