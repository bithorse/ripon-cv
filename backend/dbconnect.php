<?php 


	$host = 'localhost';
	$user = '';
	$pass = '';
	$db = '';



$conn = new mysqli($host, $user, $pass,$db);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$districts = array();
$divisions = getDivisions();


$l = 0;
foreach ($divisions as $division) {
	
	$dist = getDistricts($division);
	$districts["{$division}"] = $dist;
	$l++;
	
}

$l = 0;
$thana = array();
foreach ($districts as $key => $district_s) {
	$thana["$key"] = array();
	foreach ($district_s as $district){
		$thana["$key"]["$district"]=getThana("$district");
	}
}


$thana = json_encode($thana);
echo $thana;




function getDivisions(){
	global $conn;
	$result = $conn->query("SELECT DISTINCT division FROM districts");
	$return = array();

	while($row = $result->fetch_assoc()){
		array_push($return, $row['division']);
	}

	return $return;
}

function getDistricts($div){
	global $conn;
	$result = $conn->query("SELECT DISTINCT district FROM districts WHERE division = '{$div}'");
	$return = array();

	while($row = $result->fetch_assoc()){
		array_push($return, $row['district']);
	}

	return $return;
}
function getThana($dist){
	global $conn;
	$result = $conn->query("SELECT DISTINCT thana FROM districts WHERE district = '{$dist}'");
	$return = array();

	while($row = $result->fetch_assoc()){
		array_push($return, $row['thana']);
	}

	return $return;
}

function ap($array){
	echo "<pre>";
	print_r($array);
	echo "</pre>";
}

?>